resource "azurerm_resource_group" "cb-testdeploy" {
  name     = "cb-testdeploy"
  location = "southcentralus"
}

resource "azurerm_storage_account" "cb-testdeploy" {
  name                     = "testdeploysa"
  resource_group_name      = azurerm_resource_group.cb-testdeploy.name
  location                 = azurerm_resource_group.cb-testdeploy.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "cb-testdeploy" {
  name                = "testdeploysp"
  resource_group_name      = azurerm_resource_group.cb-testdeploy.name
  location                 = azurerm_resource_group.cb-testdeploy.location
  kind                = "FunctionApp"
  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_servicebus_namespace" "cb-testdeploy" {
  name                = "testdeploy-sevicebus"
  location            = azurerm_resource_group.cb-testdeploy.location
  resource_group_name = azurerm_resource_group.cb-testdeploy.name
  sku                 = "Standard"

  tags = {
    source = "terraform"
  }
}

resource "azurerm_servicebus_queue" "cb-testdeploy" {
  name                = "digital-deployment-partner"
  resource_group_name = azurerm_resource_group.cb-testdeploy.name
  namespace_name      = azurerm_servicebus_namespace.cb-testdeploy.name

  enable_partitioning = true
}

resource "azurerm_storage_queue" "primary-queue" {
  name                 = "slow-train"
  resource_group_name  = azurerm_resource_group.cb-testdeploy.name
  storage_account_name = azurerm_storage_account.cb-testdeploy.name
}
resource "azurerm_storage_queue" "secondary-queue" {
  name                 = "the-hard-truth"
  resource_group_name  = azurerm_resource_group.cb-testdeploy.name
  storage_account_name = azurerm_storage_account.cb-testdeploy.name
}

resource "azurerm_storage_container" "cb-testdeploy" {
  name                  = "function-releases"
  storage_account_name  = azurerm_storage_account.cb-testdeploy.name
  container_access_type = "private"
}

resource "local_file" "foo" {
    content     = "foo!"
    filename = "${path.module}/foo.bar"
}


resource "azurerm_storage_blob" "cb-testdeploy" {
  name = "functionapp.zip"
  storage_account_name   = azurerm_storage_account.cb-testdeploy.name
  storage_container_name = azurerm_storage_container.cb-testdeploy.name
  type   = "block"
  source = "${path.cwd}/functionapp.zip"
}

data "azurerm_storage_account_sas" "cb-testdeploy" {
  connection_string = azurerm_storage_account.cb-testdeploy.primary_connection_string
  https_only        = false
  resource_types {
    service   = false
    container = false
    object    = true
  }
  services {
    blob  = true
    queue = false
    table = false
    file  = false
  }
  start  = "2019-11-21"
  expiry = "2019-12-31"
  permissions {
    read    = true
    write   = false
    delete  = false
    list    = false
    add     = false
    create  = false
    update  = false
    process = false
  }
}

resource "azurerm_function_app" "cb-testdeploy" {
  name                      = "testdeployfa"
  location                  = azurerm_resource_group.cb-testdeploy.location
  resource_group_name       = azurerm_resource_group.cb-testdeploy.name
  app_service_plan_id       = azurerm_app_service_plan.cb-testdeploy.id
  storage_connection_string = azurerm_storage_account.cb-testdeploy.primary_connection_string

 app_settings = {
   "HASH" = "${base64sha256(file("${path.cwd}/functionapp.zip"))}"
   "WEBSITE_RUN_FROM_PACKAGE" = "https://${azurerm_storage_account.cb-testdeploy.name}.blob.core.windows.net/${azurerm_storage_container.cb-testdeploy.name}/${azurerm_storage_blob.cb-testdeploy.name}${data.azurerm_storage_account_sas.cb-testdeploy.sas}"
   "FUNCTIONS_WORKER_RUNTIME" = "dotnet"
   "https_only" = false
 } 
}
