
variable "tenantID" {
  default="92c085a2-3bee-438f-837f-c4bb61a3bdcb"
}

variable "location" {
  default="southcentralus"
}

variable "resourceGroupName" {
    default="CBBuddDev"
}

variable "subscriptionID" {
    default="dff0f7d2-f481-4cb7-9714-9f6731eab28f"
}

variable "servicePrincipalClientID" {
    default="1f9652ce-9d73-4e44-b9d7-8d05c55d4efd"
}
variable "servicePrincipalClientSecret" {
    default=""
}

variable "existingKeyVaultResourceID" {
    default="/subscriptions/dff0f7d2-f481-4cb7-9714-9f6731eab28f/resourceGroups/CBBuddDev/providers/Microsoft.KeyVault/vaults/YaddaKeyVault"
}