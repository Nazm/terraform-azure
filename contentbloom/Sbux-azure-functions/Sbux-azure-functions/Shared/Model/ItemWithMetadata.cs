﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class ItemWithMetadata : Item
    {
        Dictionary<String, List<String>> metadata { get; set; }
    }
}
