﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Component : ItemWithMetadataAndKeywords
    {
        public String contentType { get; set; }

        public String schema { get; set; }
    }
}
