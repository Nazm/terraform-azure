using System;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.Services.AppAuthentication;
using System.Threading.Tasks;

using System.Linq;
using Starbucks.Martech.DigitalContent.Deployment.Akamai;
using System.Collections.Generic;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared
{
    public class AzureKeyVaultService
    {
		AzureServiceTokenProvider azureServiceTokenProvider;
        KeyVaultClient keyVaultClient;

        public AzureKeyVaultService()
        {

            this.azureServiceTokenProvider = new AzureServiceTokenProvider();
            this.keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));


        }
		
        public async Task<AkamaiSecretsMapping> GetAkamaiSecretValues()
        {
            
            AkamaiSecretsMapping secretValues = new AkamaiSecretsMapping();
            AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
            //var keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));


            string target =  EnvironmentVariables.AzureKeyVaultDns + EnvironmentVariables.AzureKeyVaulLookintoDir;
            secretValues.AccessToken = (await keyVaultClient.GetSecretAsync(target + EnvironmentVariables.AkamaiAccessTokenName).ConfigureAwait(false)).Value;
            secretValues.ClientSecret = (await keyVaultClient.GetSecretAsync(target + EnvironmentVariables.AkamaiClientSecretName).ConfigureAwait(false)).Value;
            secretValues.ClientToken = (await keyVaultClient.GetSecretAsync(target + EnvironmentVariables.AkamaiClientTokenName).ConfigureAwait(false)).Value;
            secretValues.Host = (await keyVaultClient.GetSecretAsync(target + EnvironmentVariables.AkamaiFastPurgeEndpointName).ConfigureAwait(false)).Value;

            //return JsonConvert.SerializeObject(secretValues);
            return secretValues;
        }
        
		public async Task<string> GetUrlFromKeyVault(string secretKey)
        {


            //slow without ConfigureAwait(false)    sbux-folioApp-webhookUrl
            //keyvault should be keyvault DNS Name 
            string target = EnvironmentVariables.AzureKeyVaultDns + EnvironmentVariables.AzureKeyVaulLookintoDir;
            string sbuxWebhookUrl = (await keyVaultClient.GetSecretAsync(target + secretKey).ConfigureAwait(false)).Value;
            
            return sbuxWebhookUrl;
        }

        public async Task<string> GetConnectionString(string queueName)
        {

            string target = EnvironmentVariables.AzureKeyVaultDns + EnvironmentVariables.AzureKeyVaulLookintoDir;
            string s = (await keyVaultClient.GetSecretAsync(target + queueName).ConfigureAwait(false)).Value;
            return s;
        }
        public async Task<List<string>> _GetConnectionString(string queueName)
        {
            List<string> queueNameandConnString = new List<string>();
            string target = EnvironmentVariables.AzureKeyVaultDns + EnvironmentVariables.AzureKeyVaulLookintoDir;
            string s = (await keyVaultClient.GetSecretAsync(target + queueName).ConfigureAwait(false)).Value;
            
            
                queueNameandConnString = s.Split(";EntityPath").ToList();
            if (queueNameandConnString[1].Contains(";"))
                queueNameandConnString[1] = queueNameandConnString[1].Substring(1, queueNameandConnString[1].IndexOf(";"));
            else
                queueNameandConnString[1] = queueNameandConnString[1].Split("=")[1];

            return queueNameandConnString;// queueNameandConnString;
        }
    }



    
}
