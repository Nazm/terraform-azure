

#-----------------------------------------------

terraform {
   required_version = ">=0.12.7"
}


resource "azurerm_resource_group" "nazsbux" {
  name     = "naz-terraform-resourcegroup"
  location = "Central US"
}
/*##ACTIVE DIR WORKS
resource "azuread_application" "example" {
  name = "my-application"
}

resource "azuread_service_principal" "example" {
  application_id = "${azuread_application.example.application_id}"
}

resource "azuread_service_principal_password" "example" {
  service_principal_id = "${azuread_service_principal.example.id}"
  value                = "bd018069-622d-4b46-bcb9-2bbee49fe7d9"
  end_date             = "2020-01-01T01:02:03Z"
}
*/
#----------------------KEYVAULT WORKS-------------------
 # resource "azurerm_key_vault" "NazKeyVaultBYTerraform" {
#   name                = "NazKeyVaultBYTerraform"
#   location            = "${azurerm_resource_group.example.location}"
#   resource_group_name = "${azurerm_resource_group.example.name}"
#   tenant_id           = "d22b8b9e-9cee-4785-b3f7-802412127bcf"
  
#   sku_name = "standard"
  
#   access_policy {
#     tenant_id = ""
#     object_id = ""//logged in user's object id to see in portal

#     secret_permissions   = [ "List", "Get", "Set", "Delete" ]
#   }
# }


# resource "azurerm_key_vault_secret" "KeyVaultSecretBYTerraform" {
#   name         = "ccu-access-token"
#   value        = "abc"
#   key_vault_id = "${azurerm_key_vault.NazKeyVaultBYTerraform.id}"
# }


# ###--------------------SERVICE BUS WORKS------------------
# resource "azurerm_servicebus_namespace" "Servicebus_namespace" {
#    name                 = "naz-terraform-servicebus"
#    resource_group_name  = "${azurerm_resource_group.example.name}"
#    location             = "${azurerm_resource_group.example.location}"
#    sku                  = "Standard"
#    capacity             = "0"
   
# }
# resource "azurerm_servicebus_queue" "deployment-queues" {
#    name                 = "Test-queue"
#    resource_group_name  = "${azurerm_resource_group.example.name}"
#    namespace_name       = "${azurerm_servicebus_namespace.Servicebus_namespace.name}"

#    dead_lettering_on_message_expiration    = true
#    default_message_ttl                     = "PT15M"
#    duplicate_detection_history_time_window = "PT5M"
#    enable_express                          = false
#    enable_partitioning                     = false
#    lock_duration                           = "PT1M"
#    max_delivery_count                      = 5
#    max_size_in_megabytes                   = 1024
#    requires_duplicate_detection            = false
#    requires_session                        = false
# }
# resource "azurerm_servicebus_queue_authorization_rule" "deployer_send" {
#    resource_group_name  = "${azurerm_resource_group.example.name}"
#    namespace_name       = "${azurerm_servicebus_namespace.Servicebus_namespace.name}"
#    queue_name           = "${azurerm_servicebus_queue.deployment-queues.name}"
#    name                 = "Test_Queue_rule"

#    listen               = false
#    send                 = true
#    manage               = false
# }


###----------------------FunctionAPP---------------------
resource "azurerm_storage_account" "nazsbux" {
  name                     = "nazsbuxstorageaccount"
  resource_group_name      = "${azurerm_resource_group.nazsbux.name}"
  location                 = "${azurerm_resource_group.nazsbux.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "nazsbux" {
  name                = "nazsbuxserviceplan"
  location            = "${azurerm_resource_group.nazsbux.location}"
  resource_group_name = "${azurerm_resource_group.nazsbux.name}"
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "nazsbux" {
  name                      = "nazsbuxazurefunctions"
  location                  = "${azurerm_resource_group.nazsbux.location}"
  resource_group_name       = "${azurerm_resource_group.nazsbux.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.nazsbux.id}"
  storage_connection_string = "${azurerm_storage_account.nazsbux.primary_connection_string}"
}