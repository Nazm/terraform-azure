# deploying to a cb sandbox

There is a sandbox variables file now located at `contentbloom\sandboc-cb.tfvars`

|Argument|Value|
|:------:|:---:|
|var-file|`..\..\contentbloom\sandbox-cb.tfvars`|
|out|`..\..\contentbloom\deployment.tfplan`|

```
cd topologies\digitalcontent-deployment

az login

az account set --subscription "dff0f7d2-f481-4cb7-9714-9f6731eab28f"

terraform plan --var-file=..\..\contentbloom\sandbox-cb.tfvars --out=..\..\contentbloom\deployment.tfplan

terraform apply "..\..\contentbloom\deployment.tfplan"
```

Changes to SBUX Code to run in our sandbox:
* create `..\..\contentbloom\sandbox-cb.tfvars` and use for deployment
* the sandbox admins is currently set to `object_ids` as opposed to names because I got errors on the sandbox with email addresses-- TODO config issue in the Sandbox?
* find this in `topologies\digitalcontent-secrets\main.tf`
    ```
    data "azuread_users" "admins" {
    user_principal_names       = var.admins
    }
    ```
    and change it to this:
    ```
    data "azuread_users" "admins" {
        # user_principal_names       = var.admins
        object_ids                 = var.admins
    }
    ```
* comment out the following block in `topologies\digitalcontent-deployment\main.tf`, it will pick up what is necessary from your `az login` session
    ```
    terraform {
        required_version = ">=0.12.7"
        # backend "azurerm" {
        #     environment             = "public"
        #     resource_group_name     = "s00388nrgp1terraform"
        #     storage_account_name    = "s00388nsta1terraform"
        #     container_name          = "terraform"
        #     key                     = "digitalcontent-deployment.terraform.tfstate"
        #     subscription_id         = "8870e2a8-4802-46e5-b359-487db75050ae"
        #     tenant_id               = "ee69be27-d938-4eb5-8711-c5e69ca43718"
        # }
    }
    ```

## Function app module deliverable

The client sees this in `../functionapp/readme.md`

> ## Including this Module
> 
> ### Include Module Folder
> 
> This folder should be placed in the modules section of the digital-content terraform code base:
> 
> `<project-root>/modules/functionapp`
> 
> ### Include Module in Deployment
> 
> Code referencing this module may be included after the Azure-servicebus module in `<project-root>/topologies/digitalconent-deployment/main.tf`
> 
> Directly after the module for the `azure-servicebus` module, this code block should be included and will create the functionapp on the standard terraform apply command referenced in `<project-root>/readme.md`. See  `<project-root>/readme.md` for project-wide deployment instructions. 
> 
> ```
> module "functionapp_deployment" {
>    source                      = "./../../modules/functionapp"
>     resource_group_name         = azurerm_resource_group.content.name
>     location                    = var.location
>     tags                        = local.tags
>     admin_users                 = var.admins
> }
> ```

## Developer notes

### Developer access for user in admin role

When testing, we need to make sure admins users are included.  The user needs to be in the Active Directory associated with the Azure subscription, and then copy the user's Object ID, and add it to the admins list in `modules/functionapp/`

Object names in Azure must be globally unique, that is global as in all of Azure. Object names may contain up to 24 letters and numbers FYI. SBUX uses a naming module to generate standard names based on their naming spec.

In this project, for example, the storage account name is created according to naming convention. But it has to be globally unique. So we need to be sure to delete the existing storage in portal each time we run the script. To re-run the plan and application, you must either destroy the existing resoruces, or purge it manully in the portal while removing tfstate files in the project files.

### notes for further development

#### timeout when deploying, resource not available
From Naz: I see the reason with the error "resource not found" that occur some time.  It is because when we try to create secrets under KeyVault, for example, the KeyVault itself needs some time to create and the script to create the secrets cannot find the KeyVault. In this scenario "depends_on[...]" might solve the issue

From Budd: Curernt workaround is simply to run the same apply again when the process fails in this manner.  Keep re-running the apply command till it completes.




## Function app module

...