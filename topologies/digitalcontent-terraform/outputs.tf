output "terraform_storage_account" {
   description = "Storage account for Terraform remote state."
   value       = azurerm_storage_account.terraform.id
}
output "terraform_primary_blob_endpoint" {
   value       = azurerm_storage_account.terraform.primary_blob_endpoint
}
output "terraform_secondary_blob_endpoint" {
   value       = azurerm_storage_account.terraform.secondary_blob_endpoint
}
