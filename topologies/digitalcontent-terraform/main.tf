terraform {
   required_version = ">=0.12.7"
}
provider "azurerm" {
   version                     = "=1.33.0"
   environment                 = "public"
}
data "azurerm_client_config" "current" {}
data "azuread_users" "admins" {
   user_principal_names       = var.admins
}
module "naming" {
   source                      = "./../../modules/starbucks-naming-convention"
   environment                 = "non-prod"
   location                    = "westus"
   fgid                        = "00388"
}
locals {
   tags = {
      appname                    = "Martech Terraform"
      appdescription             = "Terraform remote state for Martech DevOps"
      costcenter                 = "802450"
      dataclassification         = "level2"
      environment                = "non-prod"
      fgid                       = "00388"
      location                   = "westus"
      objectname                 = "(not set)"
      ppm                        = ""
      support                    = "dl-sdlcms-issues@starbucks.com"
   }
}
resource "azurerm_resource_group" "terraform" {
   name                          = format("%s%s", module.naming.resource_group_basename, "terraform")
   location                      = "westus"
   tags                          = merge(
      local.tags, {
         objectname              = format("%s%s", module.naming.resource_group_basename, "terraform")
      }
   )
}
resource "azurerm_storage_account" "terraform" {
   name                          = format("%s%s", module.naming.storage_account_basename, "terraform")
   resource_group_name           = azurerm_resource_group.terraform.name
   location                      = azurerm_resource_group.terraform.location
   account_tier                  = "Standard"
   account_kind                  = "StorageV2"
   account_replication_type      = "GRS"
   access_tier                   = "Cool"
   enable_blob_encryption        = "true"
   enable_file_encryption        = "true"
   enable_https_traffic_only     = "true"
   tags                          = merge(
      local.tags, {
         objectname              = format("%s%s", module.naming.storage_account_basename, "terraform")
      }
   )
}
resource "azurerm_storage_container" "terraform" {
   name                          = "terraform"
   storage_account_name          = azurerm_storage_account.terraform.name
   container_access_type         = "private"
}