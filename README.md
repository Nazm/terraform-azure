# digitalcontent-terraform
Terraform Infrastructure-as-Code for Azure

## Prerequisites
Requires [terraform](https://terraform.io/) v0.12.7 or later. \
Example:
```
terraform plan -var-file=non-prod.tfvars
```
 \

Requires [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) command line utility v2.0.72 or later. \
Log in to Azure as one of the `m-` accounts:
```
az login
```
Since the `m-` accounts are shared, it may help to log in to Azure using device codes:
```
az login --use-device-code
```
 \
Make sure that the default subscription is set to Martech, not one of the oversight/shared services subscriptions:
```
az account list
az account set --subscription [name]
```
# Using `terraform`
## Workspaces
For terraform to keep consistent state over infrastructure components, state needs to be maintained in dedicated workspaces per topology and environment. Naming convention is `[topology]-[env]`, such as: 
- `secrets-nonprod` which is the workspace that contains the resources from `topology/digitalcontent-secrets` for the FGID 00388 `non-prod` Azure sbuscription;
- `deployment-prod` which is the workspace that contains the resources from `topology/digitalcontent-deployment` for the FGID 00386 `production` Azure subscription.

`terrafork workspace list` shows available workspaces. The _default_ workspace should not be used. Doing so will lead to infrastructure components being destroyed.
Use `terrawork workspace select [topology]-[env]` and/or `terraform workspace new [topology]-[env]` to select or create the workspace you want to work with.

## Environments
### Non-production
Represents Azure subscription `s00388dsubnpmartnp` for Martech (FGID `00388`). You need to be logged in to Azure with a `m-00388` administrative account for Terraform to work.
### Production
Represents Azure subscription `s00386dsubpmartp` for Martech production (FGID `00386`). You need to be logged in to Azure with a `m-00386` administrative account for Terraform to work.
### Sandbox
Represents the Sandbox Azure subscription for general learning and exploration. Log in to Azure portal with your Starbucks network account to use terraform.

### Environment variable files
Environment specific values for terraform variables (resource naming, administrative accounts, region, etc.) are predefined in `.tfvars` variable files:\
`sandbox.tfvars` contains the environment variables for Azure sandbox subscription. `non-prod.tfvars` contains the environment variables for Azure non-production subscription (i.e. FGID `00388`). And `prod.tfvars` contains the environment variables for Azure production subscription (i.e. FGID `00386`).

## Topologies
### topologies/digitalcontent-terraform/
Creates the Azure resource group and storage account needed for the Terraform [remote state](https://www.terraform.io/docs/state/remote.html) backend. The backend is created in Martech non-production FGID `00388` using Azure blob storage.\
_Only used to setup initial backend state storage._
 
### topologies/digitalcontent-secrets/
Creates the Azure Key Vaults for SDL Tridion CMS and Akamai secrets.

### topologies/digitalcontent-deployment/
Creates the Azure ServiceBus for content deployment. Sets up the message queue for SDL Tridion Deployer and the domain specific queues for Akamai invalidation, GDP Folio web hook, etc.

### Supporting modules
#### modules/starbucks-naming-convention/
Implements the Starbucks naming convention for Azure:\
https://docs.starbucks.net/display/CS/Azure+Naming+and+Tagging+Standard

#### modules/azure-servicebus/
Creates an Azure Service Bus namespace and message queues for SDL Tridion deployment. Queue connection strings are stored in Azure Key Vault with access policies.


# Putting it all together

### Setting up SDL deployment queues in non-prod
Login to Azure using an `m-00388` account:
```
az login --use-device-code
az account list
az account set --subscription s00388dsubnpmartnp
```
Navigate to the deployment queues topology:
```
cd topologies/digitalcontent-deployment/
```
Initialize terraform, select the correct workspace and plan the infrastructure: 
```
terraform init
terraform workspace select deployment-nonprod
terraform plan --var-file=../../non-prod.tfvars --out=/home/user/deployment.tfplan
```
If the provisioning plan looks correct, apply the infrastructure plan:
```
terraform apply --input=/home/user/deployment.tfplan
```
