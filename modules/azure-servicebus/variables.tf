variable "resource_group_name" {
   type        = string
   description = "Parent resource group name"
}
variable "name" {
   type        = string
   description = "Azure ServiceBus namespace"
}
variable "sizing" {
   description = "Service tier (Basic, Standard or Premium) and capacity (1, 2 or 4 for Premium tier, Basic and Standard always 0)"
   type        = object({
      sku                  = string
      capacity             = number
   })
   default = {
      sku      = "Basic"
      capacity = 0
   }
}
variable "queues" {
   description = "Queues to create in the ServiceBus namespace"
   type        = object({
      deployment           = list(string)
      notification_domains = list(string)
   })
}
variable "tags" {
   type        = map
   description = "Tags to assign to resources"
}
variable "admin_users" {
   type        = list(string)
   description = "User principal names of administrative users"
}