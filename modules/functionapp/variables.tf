variable "location" {
    type = string
    description = "location"
}

variable "tier" {
    type = string
    default = "Standard"
}

variable "resource_group_name" {
   type        = string
   description = "Parent resource group name"
}

# variable "storage_acc_name" {
#    type        = string
#    description = "Storage account name"
# }

# variable "serviceplan_name" {
#    type        = string
#    description = "Service Plan name"
# }

variable "sizing" {
   description = "Service tier (Basic, Standard or Premium) and capacity (1, 2 or 4 for Premium tier, Basic and Standard always 0)"
   type        = object({
      sku                  = string
      capacity             = number
   })
   default = {
      sku      = "Basic"
      capacity = 0
   }
}

variable "tags" {
   type        = map
   description = "Tags to assign to resources"
}

variable "admin_users" {
   type        = list(string)
   description = "User principal names of administrative users"
}

variable "key_vault_id" {
   type        = string
   description    = "Key vault id for adding secret from other module"
}
variable "keyvault_non_queue_names_dev" {
   type        = list(string)
   description = "Names of non Queue secrets in Key Vault"  
}
variable "keyvault_non_queue_values_dev" {
   type        = list(string)
   description = "Values of non Queue secrets in Key Vault in sequence of list: keyvault_non_queue_names_dev"  
}