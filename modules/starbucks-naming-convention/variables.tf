variable "environment" {
   type        = string
   description = "Azure environment: sandbox, non-prod, prod"
}
variable "location" {
   type        = string
   description = "Azure location: eastus, westus"
}
variable "fgid" {
   type        = string
   description = "Starbucks fgid"
}